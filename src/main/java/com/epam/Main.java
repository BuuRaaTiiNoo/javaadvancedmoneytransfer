package com.epam;

import com.epam.DTO.Account;
import com.epam.Service.ServiceImpl.AccountServiceImpl;
import com.epam.Transfer.AccountTransfer;
import com.epam.Transfer.Transfer;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class);

    private static Generator generator = new Generator();

    private static List<Account> accounts;

    static {
        try {
            accounts = new AccountServiceImpl().getListAccount();
        } catch (IOException e) {
            LOGGER.error("Error: ", e);
        }
    }

    private static final int NUMBER_ACCOUNT = accounts.size();

    private static Transfer transfer = new Transfer();

    private static AccountTransfer accountTransfer = new AccountTransfer(transfer);

    private static ExecutorService executor = Executors.newFixedThreadPool(NUMBER_ACCOUNT);

    public static void main(String[] args) {

        accounts.forEach(LOGGER::info);

        LOGGER.info(accounts.stream().mapToLong(Account::getBalance).sum());

        for (int i = 0; i < NUMBER_ACCOUNT; i++) {
            executor.execute(accountTransfer);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();

//        List<Thread> list = new ArrayList<>();
//        for (int i = 0; i < NUMBER_ACCOUNT; i++) {
//            list.add(new Thread(accountTransfer));
//        }
//        list.forEach(Thread::start);
        transfer.getAccounts().forEach(LOGGER::info);
        LOGGER.info(transfer.getAccounts().stream().mapToLong(Account::getBalance).sum());
    }
}