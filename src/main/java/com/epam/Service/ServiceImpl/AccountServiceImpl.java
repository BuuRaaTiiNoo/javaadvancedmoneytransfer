package com.epam.Service.ServiceImpl;

import com.epam.DTO.Account;
import com.epam.Repository.AccountRepository;
import com.epam.Repository.RepositoryImpl.AccountRepositoryImpl;
import com.epam.Service.AccountService;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository = new AccountRepositoryImpl();

    @Override
    public void write(Account account) {
        accountRepository.write(account);
    }

    @Override
    public Account read(Path path) {
        return accountRepository.read(path);
    }

    @Override
    public List<Path> getListPathAccounts() throws IOException {
        return accountRepository.getListPathAccounts();
    }

    @Override
    public List<Account> getListAccount() throws IOException {
        return accountRepository.getListAccount();
    }

    @Override
    public long getTotalSum() throws IOException {
        return accountRepository.getTotalSum();
    }

    @Override
    public void addAccount(Account account) {
        accountRepository.addAccount(account);
    }

    @Override
    public List<Account> getAccounts() {
        return null;
    }
}
