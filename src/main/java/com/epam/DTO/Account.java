package com.epam.DTO;

import java.io.Serializable;

public class Account implements Serializable {
    private long id;
    private String accountName;
    private long balance;

    public Account() {
    }

    public Account(long id, String accountName, long balance) {
        this.id = id;
        this.accountName = accountName;
        this.balance = balance;
    }

    public void refill(long sum) {
        this.balance += sum;
    }

    public void debit(long sum) {
        this.balance -= sum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }


    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", accountName='" + accountName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
