package com.epam.Transfer;

import com.epam.DTO.Account;
import com.epam.Service.ServiceImpl.AccountServiceImpl;
import com.epam.exception.NotEnoughMoneyException;
import com.epam.exception.SameAccountException;
import org.apache.log4j.Logger;

import javax.transaction.TransactionRequiredException;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Transfer {

    private static final Logger LOGGER = Logger.getLogger(Transfer.class);

    private static final int TOTAL_NUMBER_OF_TRANSFER = 1000;

    private static List<Account> accounts;

    static {
        try {
            accounts = new AccountServiceImpl().getListAccount();
        } catch (IOException e) {
            LOGGER.error("Ошибка чтения данных из файла.");
        }
    }

    private static final int NUMBER_ACCOUNT = accounts.size();

    private AtomicInteger atomicInteger = new AtomicInteger();

    private void checkedTransferOnValid(int from, int to, int amount) {

        if (from == to) try {
            throw new SameAccountException();
        } catch (SameAccountException e) {
            LOGGER.error("Ошибка перевода, вы пытаетесь перевести " + amount + " на свой же счет ");
        }
        if (amount == 0) try {
            throw new TransactionRequiredException();
        } catch (TransactionRequiredException e) {
            LOGGER.error("Ошибка перевода, сумма перевода не должна быть равна " + amount);
        }
    }

    private void doTransfer(int from, int to, int amount) {

        if (accounts.get(from).getBalance() < amount) {
            try {
                throw new NotEnoughMoneyException();
            } catch (NotEnoughMoneyException e) {
                LOGGER.error("На машем счету баланс " + accounts.get(from).getBalance() + ", этого не достаточно для перевода sum: " + amount);
            }
        } else {
            if (atomicInteger.get() < TOTAL_NUMBER_OF_TRANSFER) {
                atomicInteger.incrementAndGet();
                accounts.get(from).debit(amount);
                accounts.get(to).refill(amount);
                LOGGER.info("From " + accounts.get(from).toString() + "\t " + " sum: " + amount + "\t to " + accounts.get(to).toString());
            } else {
                Thread.currentThread().interrupt();
            }
        }
    }

    public void transfer() {
        int from = new Random().nextInt(NUMBER_ACCOUNT);
        int to = new Random().nextInt(NUMBER_ACCOUNT);
        int amount = new Random().nextInt(1000);
        checkedTransferOnValid(from, to, amount);
        if (from > to) {
            synchronized (accounts.get(from)) {
                synchronized (accounts.get(to)) {
                    doTransfer(from, to, amount);
                }
            }
        } else {
            synchronized (accounts.get(to)) {
                synchronized (accounts.get(from)) {
                    doTransfer(from, to, amount);
                }
            }
        }
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}
