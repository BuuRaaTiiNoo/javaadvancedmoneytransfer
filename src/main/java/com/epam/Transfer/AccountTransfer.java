package com.epam.Transfer;


public class AccountTransfer implements Runnable {

    private final Transfer transfer;

    public AccountTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            transfer.transfer();
        }
    }
}