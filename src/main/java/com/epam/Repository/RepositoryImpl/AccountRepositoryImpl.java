package com.epam.Repository.RepositoryImpl;

import com.epam.DTO.Account;
import com.epam.Repository.AccountRepository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AccountRepositoryImpl implements AccountRepository {

    private List<Account> accounts = new ArrayList<>();

    //Write objects Account type in file account+ID.ac, as parameter give account.
    @Override
    public void write(Account account) {
        try (FileOutputStream fileOutputStream = new FileOutputStream("src/main/java/com/epam/Accounts/account" + account.getId() + ".ac");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(account);
            objectOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Считываем из файла в обьект, в качестве параметра передаем путь к файлу.
    @Override
    public Account read(Path path) {
        try (InputStream inputStream = new FileInputStream(path.toString());
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            return (Account) objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Получаем список файлов из директории Accounts.
    @Override
    public List<Path> getListPathAccounts() throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get("src/main/java/com/epam/Accounts"))) {
            return paths
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        }
    }

    //Получаем список аккаунтов
    @Override
    public List<Account> getListAccount() throws IOException {
        for (Path paths : getListPathAccounts()) {
            accounts.add(Objects.requireNonNull(read(paths)));
        }
        return accounts;
    }

    //Получаем общий баланс до совершения операций перевода
    @Override
    public long getTotalSum() throws IOException {
        long sum = 0;
        for (Path paths : getListPathAccounts()) {
            sum += Objects.requireNonNull(read(paths)).getBalance();
        }
        return sum;
    }

    @Override
    public void addAccount(Account account) {
        accounts.add(account);
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}
