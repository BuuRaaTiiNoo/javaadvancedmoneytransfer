package com.epam.Repository;

import com.epam.DTO.Account;


import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface AccountRepository {
    public void write(Account account);

    public Account read(Path path);

    public List<Path> getListPathAccounts() throws IOException;

    public List<Account> getListAccount() throws IOException;

    public long getTotalSum() throws IOException;

    public void addAccount(Account account);
}
