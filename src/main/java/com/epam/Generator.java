package com.epam;

import com.epam.DTO.Account;
import com.epam.Service.ServiceImpl.AccountServiceImpl;

import java.util.Random;

public class Generator {
    private static final String[] name = {
            "Kolya", "Ivan", "Sasha",
            "Petya", "Vasya", "Ibragim",
            "Masha", "Anna", "Olya", "Gesha"
    };

    private AccountServiceImpl accountService = new AccountServiceImpl();

    public void generateAccount() {
        for (int i = 0; i < name.length; i++)
            accountService.write(new Account(i, name[i], new Random().nextInt(5000)));
    }

    public AccountServiceImpl getAccountService() {
        return accountService;
    }
}
